/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal1.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

/**
 *
 * @author st
 */
public class Kot implements Animal{
        private String species;
    private String status;
    private String name;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public Kot(){
        this.species="Kot";
        this.name="Mruczek";
    }
     @Override
    public String getSpecies() {
       return species;
    }

    @Override
    public String getName() {
       return name;
    }

    @Override
    public String getStatus() {
       return status;
    }

    @Override
    public void setStatus(String status) {
      this.pcs.firePropertyChange("zmiana statusu", this.status, status);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
       this.pcs.removePropertyChangeListener(listener);
    } 
}
