/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal3.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

/**
 *
 * @author st
 */
public class Activator implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
         Logger.get().log(this, "Foka się pojawiła");
        bc.registerService(Animal.class.getName(), new Foka(), null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        ServiceReference ref = bc.getServiceReference(Animal.class.getName());
        Logger.get().log(this, "Foka odpłynęła");
        bc.ungetService(ref);
    }
    
}
