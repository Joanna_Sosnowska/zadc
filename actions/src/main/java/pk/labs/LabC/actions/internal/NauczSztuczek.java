/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.actions.internal;

import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

/**
 *
 * @author st
 */
public class NauczSztuczek implements AnimalAction{

    @Override
    public boolean execute(Animal animal) {
       if(animal!=null && !"Kot".equals(animal.getSpecies())){
            animal.setStatus(animal.getSpecies()+" uczy się sztuczek");
            return true;
       }
       else if (!"Kot".equals(animal.getSpecies())){
           animal.setStatus("Kot strzela focha");
       }
       return false;
    }
    
}
