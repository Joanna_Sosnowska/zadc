/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.actions.internal;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.AnimalAction;

/**
 *
 * @author st
 */
public class Actions implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
        Map t1 = new Hashtable ();
        Map  t2 = new Hashtable ();
        Map  t3 = new Hashtable ();
        
        t1.put("Kot", new String[]{"kotek"});
        t2.put("Pies", new String[]{"piesek"});
        t3.put("Foka", new String[]{"foczka"});
        
        t1.put("name", "specAnimal1");
        t2.put("name", "specAnimal3");
        t3.put("name", "specAnimal3");
        
        bc.registerService(AnimalAction.class.getName(), new Nakarm(), (Dictionary) t1);
        bc.registerService(AnimalAction.class.getName(), new NauczSztuczek(), (Dictionary) t2);
        bc.registerService(AnimalAction.class.getName(), new PobawSie(), (Dictionary) t3);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
       
    }
    
}
