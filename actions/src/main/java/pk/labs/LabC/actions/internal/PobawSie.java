/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.actions.internal;

import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

/**
 *
 * @author st
 */
public class PobawSie implements AnimalAction{
    @Override
    public boolean execute(Animal animal) {
       if(animal!=null){
            animal.setStatus(animal.getSpecies()+" bawi się");
            return true;
       }
       return false;
    }
}
